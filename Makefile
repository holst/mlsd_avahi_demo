echo_domain='echo.sd.'

named=$(shell which named)
netns=netns/netns
# echo=StatelessDNS-EchoServer/statelessDNS
reflector=mini_reflector/sdreflector.pl
avahi=mlsd_avahi/local/bin/avahi-daemon
avahi-browse=mlsd_avahi/local/bin/avahi-browse
avahi-publish=mlsd_avahi/local/bin/avahi-publish



ifdef gdb
debug=gdb $(gdb) --args
endif

all: log
	@echo -n 'Get privilege '
	@sudo echo '-> Ok'
	@$(MAKE) real_all -j

log:
	@mkdir -p log

bind:
	@echo 'Start name server'
	$(netns) -a 10.0.5.1/24 -n bind -- \
		$(named) -c named.conf -4 -g

# echo:
# 	@echo 'Start stateless echo server'
# 	@$(netns) -a 10.0.5.2/24 -n echo -- \
# 		$(echo) -m methods/ -a 10.0.5.2 -b $(echo_domain)

reflector:
	@echo 'Start minimal reflector'
	$(netns) -a 10.0.5.2/24 -n echo -- \
		$(reflector)

avahi-bob:
	@echo 'Bob starts avahi (with dbus)'
	$(netns) -a 10.0.5.3/24 -n avahi-bob -- $(debug) \
		$(avahi) --no-chroot --file avahi-daemon-bob.conf

avahi-alice:
	@echo 'Alice starts avahi (without dbus)'
	$(netns) -a 10.0.5.4/24 -n avahi-alice -- $(debug) \
		$(avahi) --no-chroot --file avahi-daemon-alice.conf

avahi-discover:
	@echo 'Service browsing'
	$(netns) -a 10.0.5.5/24 -n discover -- \
		avahi-discover

avahi-browse:
	@echo 'Service browsing'
	$(netns) -a 10.0.5.6/24 -n discover -- \
		$(avahi-browse) -ar
# $(avahi-browse) -a -d 'zeroconf.org'

avahi-publish:
	@echo 'Publish service'
	$(netns) -a 10.0.5.33/24 -n publish -- \
		$(avahi-publish) -s new _presence._tcp 3455

real_all: bind echo

console:
	$(netns) -a 10.0.5.254/24 -n shell -- sudo -u $(USER) bash

.PHONY: console bind echo all real_all
