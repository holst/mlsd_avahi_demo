# Description

Setup a demo environment. Requires a Bind Name Server in path and linux network namespace support.

# Build

*   First get submodules:
    ~~~bash
    git submodule update --recursive --init
    ~~~
    
*   Compile avahi with mlsd extention:
    ~~~bash
    cd mlsd_avahi
    ./mlsd_bootstrap.sh
    cd ..
    ~~~

# Demo

*   Important! Add 10.0.5.1 as first name server in */etc/resolv.conf*.

*   Adapt listen IP of the reflector:
    ~~~bash
    sed -i '/LocalAddr/s/".*"/"10.0.5.2"/' mini_reflector/sdreflector.pl
    ~~~

*   Call following make targets in separate shells:
    ~~~bash
    make bind
    make reflector
    make avahi-bob
    make avahi-alice
    ~~~
    
*   To browse the available services, start `make avahi-browse` or for manual lookup start a shell `make console`.

*   Further demos are listed in the [Makefile](Makefile)
